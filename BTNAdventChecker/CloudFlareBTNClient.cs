﻿using System;
using System.Net;
using System.IO;
using System.Web;
using System.Text.RegularExpressions;

using V8.Net;

namespace BTNAdventChecker
{
    public class CloudFlareBTNClient : WebClient
    {
        private CookieContainer outboundCookies = new CookieContainer();
        private CookieCollection inboundCookies = new CookieCollection();

        private static Cookie cfduid = null;
        private static Cookie cf_clearance = null;
        private static Cookie PHPSESSID = null;
        private static Cookie keeplogged = null;

        public CookieContainer OutboundCookies
        {
            get { return outboundCookies; }
            set { outboundCookies = value; }
        }

        public CookieCollection InboundCookies
        {
            get { return inboundCookies; }
        }

        public bool IgnoreRedirects { get; set; }

        public CloudFlareBTNClient()
        {
            
            if (keeplogged == null)
                keeplogged = FindBTNCookie();
            if (keeplogged == null)
                throw new Exception("NotAuthenticated");
        }

        public static void Init()
        {
            keeplogged = FindBTNCookie();
        }

        public static bool IsAuthenticated()
        {
            if (keeplogged == null)
                return false;
            return true;
        }

        protected override WebRequest GetWebRequest(Uri address)
        {
            WebRequest request = base.GetWebRequest(address);
            if (request is HttpWebRequest)
            {
                CookieContainer container = outboundCookies;
                if (cfduid != null)
                    container.Add(cfduid);
                if (cf_clearance != null)
                    container.Add(cf_clearance);
                if (PHPSESSID != null)
                    container.Add(PHPSESSID);
                if (keeplogged != null)
                    container.Add(keeplogged);
                (request as HttpWebRequest).CookieContainer = container;
                (request as HttpWebRequest).AllowAutoRedirect = false;
                (request as HttpWebRequest).UserAgent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/34.0.1847.116 Chrome/34.0.1847.116 Safari/537.36";
            }
            return request;
        }

        protected override WebResponse GetWebResponse(WebRequest request)
        {
            WebResponse response = HandleRequest(request);
            StreamReader sr = new StreamReader(response.GetResponseStream());
            String resp = sr.ReadToEnd();
            sr.DiscardBufferedData();        
           
            if (resp.Contains("a = document.getElementById('jschl-answer');"))
            {
                Uri solutionUri = SolveCFChallenge(resp);
                WebRequest req = GetWebRequest(solutionUri);
                ((HttpWebRequest)req).Referer = response.ResponseUri.ToString();
                response = HandleRequest(req);   
            }            

            return response;
        }

        private WebResponse HandleRequest(WebRequest request)
        {
            WebResponse response = null;
            try
            {
                response = base.GetWebResponse(request);
            }
            catch (WebException ex)
            {
                if (ex.Message.Contains("The remote name could not be resolved"))
                    return null;
                response = ex.Response;                
            }

            CookieCollection responseCookies = ((HttpWebResponse)response).Cookies;
            foreach (Cookie cookie in responseCookies)
            {
                if (cookie.Name == "__cfduid")
                    cfduid = cookie;
                if (cookie.Name == "cf_clearance")
                    cf_clearance = cookie;
                if (cookie.Name == "PHPSESSID")
                    PHPSESSID = cookie;
                if (cookie.Name == "keeplogged")
                    keeplogged = cookie;
            }

            if (((HttpWebResponse)response).StatusCode == HttpStatusCode.Found)
            {
                String location = response.Headers["Location"];
                //if (location.Contains("login.php"))
                    //return null; // throw new NotAuthenticatedException("No BTN cookie found.");
                Console.WriteLine(location);
                WebRequest req = null;
                try
                {
                    req = GetWebRequest(new Uri(location));
                }
                catch (UriFormatException ex)
                {
                    Uri locationUri = new Uri(((HttpWebResponse)response).ResponseUri, location);
                    req = GetWebRequest(locationUri);
                }
               // ((HttpWebRequest)req).Referer = response.ResponseUri.ToString();
                //req.Headers.Add(HttpRequestHeader.Referer, response.Headers["Location"]);
                return HandleRequest(req);
            }

            return response;
        }

        private Uri SolveCFChallenge(String resp)
        {
            String challenge = RegexFindHelper(@"name=""jschl_vc"" value=""(\w+)""", resp);
            String builder = RegexFindHelper(@"setTimeout.+?\r?\n([\s\S]+?a\.value =.+?)\r?\n", resp);
            builder = Regex.Replace(builder, @"a\.value =(.+?) \+ .+?;", @"$1");
            builder = Regex.Replace(builder, @"\s{3,}[a-z](?: = |\.).+", "");

            V8Engine v8Engine = new V8Engine();
            Handle result = v8Engine.Execute(builder, "My V8.NET Console");

            String answer = Convert.ToString(((int)result) + "broadcasthe.net".Length);

            string solutionUrl = "https://broadcasthe.net/cdn-cgi/l/chk_jschl";
            UriBuilder uriBuilder = new UriBuilder(solutionUrl);
            var query = HttpUtility.ParseQueryString(uriBuilder.Query);
            query["jschl_answer"] = answer;
            query["jschl_vc"] = challenge;
            uriBuilder.Query = query.ToString();            
            return uriBuilder.Uri;            
        }

        private String RegexFindHelper(String pattern, String source)
        {
            Regex Regex = new Regex(pattern);
            Match Match = Regex.Match(source);
            if (Match.Success)
                return Match.Groups[1].Value;

            return null;
        }

        private static Cookie FindBTNCookie()
        {            
            Cookie activeCookie = null;

            activeCookie = CookieHelper.GetChromeCookie();
            if (activeCookie != null) return activeCookie;

            activeCookie = CookieHelper.GetFirefoxCookie();
            if (activeCookie != null) return activeCookie;

            /*
            activeCookie = CookieHelper.GetInternetExplorerCookie();
            if (activeCookie != null) return activeCookie;

            activeCookie = CookieHelper.GetOperaCookie();
            if (activeCookie != null) return activeCookie;            
            */

            return null;
        }
    }
}