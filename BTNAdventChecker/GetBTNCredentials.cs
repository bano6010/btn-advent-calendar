﻿using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;

namespace BTNAdventChecker
{
    public partial class GetBTNCredentials : Form
    {
        private BackgroundWorker _worker;

        public GetBTNCredentials()
        {
            InitializeComponent();
        }

        private void loginBtn_Click(object sender, EventArgs e)
        {
            _worker = new BackgroundWorker();
            _worker.DoWork += DoLoginWork;
            _worker.RunWorkerCompleted += WorkCompleted;

            usernameTxtbox.Enabled = false;
            passwordTxtbox.Enabled = false;
            loginBtn.Enabled = false;
            loggingInLbl.Visible = true;

            if (authCodeTxtbox.Visible)
                authCodeTxtbox.Enabled = false;

            progressBar1.Visible = true;
            progressBar1.Style = ProgressBarStyle.Marquee;

            _worker.RunWorkerAsync();
        }

        private void WorkCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Result.Equals("success"))
                Close();
            else if(e.Result.Equals("needCode"))
            {
                loginBtn.Enabled = true;
                loggingInLbl.Visible = false;
                progressBar1.Visible = false;
                label3.Visible = true;
                authCodeTxtbox.Visible = true;
            }
            else
            {
                usernameTxtbox.Enabled = true;
                passwordTxtbox.Enabled = true;
                loginBtn.Enabled = true;
                loggingInLbl.Visible = false;
                progressBar1.Visible = false;
                String[] errorParts = e.Result.ToString().Split(new char[] {'-'});
                MessageBox.Show(errorParts[0], errorParts[1], MessageBoxButtons.OK, MessageBoxIcon.Error);
                _worker = null;
            }
        }

        private void DoLoginWork(object sender, DoWorkEventArgs e)
        {
            CloudFlareBTNClient wc = new CloudFlareBTNClient();
            wc.IgnoreRedirects = false;
            String output = String.Empty;
            try
            {
                output = wc.DownloadString("https://broadcasthe.net/login.php");
                Console.WriteLine(output);
            }
            catch (Exception ex)
            {
                Log.Error(String.Format("There was an error checking for captcha: {0}", ex.Message));
                e.Result = "There is a network problem or the site is down. Please try again later.-Could not connect to BTN!";
                return;
            }
            Regex cap = new Regex("https://www.google.com/recaptcha/");
            Match capchaEnabled = cap.Match(output);
            if (capchaEnabled.Success)
            {
                e.Result = "ReCapcha is enabled! Please wait until later and try again!-Error logging in!";
                return;
            }

            NameValueCollection postData = new NameValueCollection();
            if (!authCodeTxtbox.Visible)
            {

                postData.Add("username", usernameTxtbox.Text);
                postData.Add("password", passwordTxtbox.Text);
                postData.Add("login", "Log In!");
                postData.Add("keeplogged", "1");
            }
            else
            {
                postData.Add("act", "authenticate");
                postData.Add("code",authCodeTxtbox.Text);   
            }

            UriBuilder loginUriBuilder = new UriBuilder();
            loginUriBuilder.Host = "broadcasthe.net";
            loginUriBuilder.Path = "login.php";
            loginUriBuilder.Scheme = "https";

            CookieCollection cookies = new CookieCollection();
            String page = String.Empty;

            using (var client = new CloudFlareBTNClient())
            {
                client.IgnoreRedirects = false;
                try
                {
                    byte[] returnPage = client.UploadValues(loginUriBuilder.Uri, "POST", postData);
                    page =  Encoding.UTF8.GetString(returnPage, 0, returnPage.Length);
                }
                catch (Exception ex)
                {
                    Log.Error(String.Format("There was an error getting cookies: {0}", ex.Message));
                    e.Result = "There is a network problem or the site is down. Please try again later.-Could not connect to BTN!";
                    return;
                }
                foreach (Cookie cookie in client.InboundCookies.OfType<Cookie>())
                    cookies.Add(cookie);
            }

            //File.WriteAllText("temp.txt",page);
            Match AuthCode = Regex.Match(page, "<title>Authenticate :: BroadcasTheNet</title>");
            if(AuthCode.Success)
            {
                e.Result = "needCode";
                return;
            }

            String ProgramData = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "BTNAdventCalendarChecker");
            if (!Directory.Exists(ProgramData)) Directory.CreateDirectory(ProgramData);
            String CookieFile = Path.Combine(ProgramData, "BTNCookies.xml");
            if (File.Exists(CookieFile)) File.Delete(CookieFile);

            XmlTextWriter writer = new XmlTextWriter(CookieFile, Encoding.UTF8);
            writer.Formatting = Formatting.Indented;
            writer.WriteStartElement("Cookies");

            foreach (Cookie cookie in cookies)
            {
                writer.WriteStartElement("Cookie");

                writer.WriteStartElement("CookieName");
                writer.WriteString(cookie.Name);
                writer.WriteEndElement();

                writer.WriteStartElement("CookieValue");
                writer.WriteString(cookie.Value);
                writer.WriteEndElement();

                writer.WriteStartElement("CookieDomain");
                writer.WriteString(cookie.Domain);
                writer.WriteEndElement();

                writer.WriteStartElement("CookieExpires");
                writer.WriteString(cookie.Expires.ToString("yyyy-MM-dd HH:mm:ss.fffffff"));
                writer.WriteEndElement();

                writer.WriteEndElement();
            }

            writer.WriteEndElement();

            writer.Close();
            e.Result = "success";
        }
    }
}
