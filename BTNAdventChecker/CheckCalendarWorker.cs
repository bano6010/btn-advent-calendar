﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Net;
using System.Xml;

namespace BTNAdventChecker
{
    class CheckCalendarWorker : BackgroundWorker
    {
        protected override void OnDoWork(DoWorkEventArgs e)
        {
            String returnData = String.Empty;

            try
            {
                CloudFlareBTNClient wc = new CloudFlareBTNClient();
                returnData = wc.DownloadString("https://broadcasthe.net/advent.php?action=claimprize");
            }
            catch(Exception ex)
            {
                e.Result = ex.Message;
                return;
            }
            
            e.Result = returnData;
        }
    }
}
