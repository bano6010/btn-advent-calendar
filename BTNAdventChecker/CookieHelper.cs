﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BTNAdventChecker
{
    class CookieHelper
    {
        private static string GetChromeCookiePath()
        {
            string s = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            s += @"\Google\Chrome\User Data\Default\Cookies";

            if (!File.Exists(s))
                return string.Empty;

            return s;
        }
        public static Cookie GetChromeCookie()
        {
            String strPath = GetChromeCookiePath();

            if (string.Empty == strPath)
                return null;

            try
            {
                String strDb = "Data Source=" + strPath + ";pooling=false";

                using (SQLiteConnection conn = new SQLiteConnection(strDb))
                {
                    using (SQLiteCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "SELECT * FROM cookies WHERE host_key = \"broadcasthe.net\" OR host_key = \".broadcasthe.net\";";

                        conn.Open();
                        using (SQLiteDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Cookie sessionCookie = new Cookie();
                                sessionCookie.Name = (string)reader[2];
                                if (string.Compare(sessionCookie.Name, "keeplogged") != 0) continue;
                                byte[] decodedData = System.Security.Cryptography.ProtectedData.Unprotect((byte[])reader[12], null, System.Security.Cryptography.DataProtectionScope.CurrentUser);
                                sessionCookie.Value = Encoding.ASCII.GetString(decodedData);
                                sessionCookie.Domain = (string)reader[1];
                                sessionCookie.Path = (string)reader[4];
                                long expiredTS = (long)reader[5];
                                System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                                sessionCookie.Expires = DateTime.Now.AddDays(1);
                                conn.Close();
                                return sessionCookie;
                            }
                        }
                        conn.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }

        private static string GetOperaCookiePath()
        {
            string s = Environment.GetFolderPath(
                Environment.SpecialFolder.ApplicationData);
            s += @"\Opera Software\Opera Stable\Cookies";

            if (!File.Exists(s))
                return string.Empty;

            return s;
        }
        public static Cookie GetOperaCookie()
        {
            //Value = string.Empty;
            //bool fRtn = false;
            string strPath, strDb;

            // Check to see if Chrome Installed
            strPath = GetOperaCookiePath();

            if (string.Empty == strPath) // Nope, perhaps another browser
                return null;

            Cookie sessionCookie = null;
            String value = String.Empty;

            try
            {
                strDb = "Data Source=" + strPath + ";pooling=false";

                using (SQLiteConnection conn = new SQLiteConnection(strDb))
                {
                    using (SQLiteCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "SELECT encrypted_value FROM cookies WHERE host_key = 'broadcasthe.net' AND name='keeplogged';";

                        conn.Open();
                        using (SQLiteDataReader reader = cmd.ExecuteReader())
                        {
                            /*
                            while (reader.Read())
                            {
                                value = reader.GetString(0);
                                if (!value.Equals(string.Empty))
                                {
                                    sessionCookie = new Cookie
                                    fRtn = true;
                                    break;
                                }
                            }
                            */
                            
                            while (reader.Read())
                            {
                                var encryptedData = (byte[])reader[0];
                                var decodedData = System.Security.Cryptography.ProtectedData.Unprotect(encryptedData, null, System.Security.Cryptography.DataProtectionScope.CurrentUser);
                                var plainText = Encoding.ASCII.GetString(decodedData); // Looks like ASCII
                                sessionCookie = new Cookie();
                                sessionCookie.Name = "keeplogged";
                                sessionCookie.Value = plainText;
                                //Value = plainText;
                                //return Tuple.Create(reader.GetString(0), plainText);
                            }



                        }
                        conn.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                //Value = string.Empty;
                //fRtn = false;
            }
            return sessionCookie;
        }        

        private static bool IsWindows7()
        {
            OperatingSystem osVersion = Environment.OSVersion;
            return (osVersion.Platform == PlatformID.Win32NT) && (osVersion.Version.Major == 6) && (osVersion.Version.Minor == 1);
        }
        public static Cookie GetInternetExplorerCookie()
        {
            //Value = string.Empty;
            //bool fRtn = false;
            string strPath, strCookie;
            string strHost = "";
            string strField = "";
            string[] fp;
            StreamReader r;
            int idx;

            try
            {
                strField = strField + "\n";
                strPath = Environment.GetFolderPath(Environment.SpecialFolder.Cookies);
                Version v = Environment.OSVersion.Version;

                /*
                if (IsWindows7())
                {
                    strPath += @"\low";
                }
                */
                fp = Directory.GetFiles(strPath, "*.txt");

                foreach (string path in fp)
                {
                    idx = -1;
                    r = File.OpenText(path);
                    strCookie = r.ReadToEnd();
                    r.Close();

                    if (System.Text.RegularExpressions.Regex.IsMatch(strCookie, strHost))
                    {
                        idx = strCookie.ToUpper().IndexOf(strField.ToUpper());
                    }

                    if (-1 < idx)
                    {
                        idx += strField.Length;
                        //Value = strCookie.Substring(idx, strCookie.IndexOf('\n', idx) - idx);
                        //if (!Value.Equals(string.Empty))
                        {
                            //fRtn = true;
                            break;
                        }
                    }
                }
            }
            catch (Exception) //File not found, etc...
            {
                //Value = string.Empty;
                //fRtn = false;
            }

            return null;
        }

        private static string GetFireFoxCookiePath()
        {
            string s = Environment.GetFolderPath(
                             Environment.SpecialFolder.ApplicationData);
            s += @"\Mozilla\Firefox\Profiles\";

            try
            {
                DirectoryInfo di = new DirectoryInfo(s);
                DirectoryInfo[] dir = di.GetDirectories("*.default");
                if (dir.Length != 1)
                    return string.Empty;

                s += dir[0].Name + @"\" + "cookies.sqlite";
            }
            catch (Exception)
            {
                return string.Empty;
            }

            if (!File.Exists(s))
                return string.Empty;

            return s;
        }
        public static Cookie GetFirefoxCookie()
        {
            //Value = string.Empty;
            bool fRtn = false;
            string strPath, strTemp, strDb;
            strTemp = string.Empty;

            // Check to see if FireFox Installed
            strPath = GetFireFoxCookiePath();
            if (string.Empty == strPath) // Nope, perhaps another browser
                return null;

            String value = String.Empty;
            Cookie sessionCookie = null;

            try
            {
                // First copy the cookie jar so that we can read the cookies from unlocked copy while
                // FireFox is running
                strTemp = strPath + ".temp";
                strDb = "Data Source=" + strTemp + ";pooling=false";

                File.Copy(strPath, strTemp, true);

                // Now open the temporary cookie jar and extract Value from the cookie if
                // we find it.
                using (SQLiteConnection conn = new SQLiteConnection(strDb))
                {
                    using (SQLiteCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "SELECT value FROM moz_cookies WHERE host='broadcasthe.net' AND name='keeplogged';";

                        conn.Open();
                        using (SQLiteDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                value = reader.GetString(0);
                                if (!value.Equals(string.Empty))
                                {
                                    sessionCookie = new Cookie();
                                    sessionCookie.Name = "keeplogged";
                                    sessionCookie.Value = value;
                                    System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                                    sessionCookie.Expires = DateTime.Now.AddDays(1);
                                    sessionCookie.Domain = "broadcasthe.net";
                                    sessionCookie.Path = "/";
                                    fRtn = true;
                                    break;
                                }
                            }
                        }
                        conn.Close();
                    }
                }
            }
            catch (Exception)
            {
                //Value = string.Empty;
                fRtn = false;
                return null;
            }

            // All done clean up
            if (string.Empty != strTemp)
            {
                File.Delete(strTemp);
            }
            return sessionCookie;
        }
    }
}