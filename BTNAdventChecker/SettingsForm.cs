﻿using System;
using System.Windows.Forms;
using Microsoft.Win32;

namespace BTNAdventChecker
{
    public partial class SettingsForm : Form
    {

        private const String AppName = "BTN Advent Calendar Checker";
        private bool ignoreStateChange = false;
        public SettingsForm()
        {
            InitializeComponent();

            //checkBox1.CheckState = () ? CheckState.Checked : CheckState.Unchecked;
            if (IsStartupItem())
                CheckCheckBox(checkBox1);

            String AlertEmail = GetAlertEmail();
            emailTxtbox.Text = AlertEmail;

            String SendAlertEmail = GetSendAlertEmail();
            //checkBox2.CheckState = (SendAlertEmail != null || String.Compare(SendAlertEmail,"0") == 1) ? CheckState.Checked : CheckState.Unchecked;
            if (SendAlertEmail != null && String.Compare(SendAlertEmail, "0") == 1)
            {
                emailTxtbox.Enabled = true;
                CheckCheckBox(checkBox2);
            }
            else
                UncheckCheckBox(checkBox2);
        }

        private void CheckCheckBox(CheckBox box)
        {
            ignoreStateChange = true;
            box.CheckState = CheckState.Checked;
            ignoreStateChange = false;
        }

        private void UncheckCheckBox(CheckBox box)
        {
            ignoreStateChange = true;
            box.CheckState = CheckState.Unchecked;
            ignoreStateChange = false;
        }

        private bool IsStartupItem()
        {
            // The path to the key where Windows looks for startup applications
            RegistryKey rkApp = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);

            if (rkApp.GetValue(AppName) == null)
            {
                // The value doesn't exist, the application is not set to run at startup
                rkApp.Close();
                return false;
            }

            // The value exists, the application is set to run at startup
            rkApp.Close();
            return true;
        }

        private void SetAlertEmail()
        {
            // The path to the key where Windows looks for startup applications
            Registry.SetValue("HKEY_CURRENT_USER\\SOFTWARE\\BTN Advent Calendar", "AlertEmail", emailTxtbox.Text, RegistryValueKind.String);
        }

        private String GetAlertEmail()
        {
            // The path to the key where Windows looks for startup applications
            RegistryKey rkApp = Registry.CurrentUser.OpenSubKey("SOFTWARE\\BTN Advent Calendar", true);
            if (rkApp == null) return null;
            String alertEmail = rkApp.GetValue("AlertEmail") as String;
            rkApp.Close();
            if (alertEmail == null)
                // The value doesn't exist, the application is not set to run at startup
                return null;

            // The value exists, the application is set to run at startup
            return alertEmail;
        }

        private void SetSendAlertEmail(String value)
        {
            // The path to the key where Windows looks for startup applications
            Registry.SetValue("HKEY_CURRENT_USER\\SOFTWARE\\BTN Advent Calendar", "SendAlertEmail", value, RegistryValueKind.String);
        }

        private String GetSendAlertEmail()
        {
            // The path to the key where Windows looks for startup applications
            RegistryKey rkApp = Registry.CurrentUser.OpenSubKey("SOFTWARE\\BTN Advent Calendar", true);
            if (rkApp == null) return null;
            String alertEmail = rkApp.GetValue("SendAlertEmail") as String;
            rkApp.Close();
            if (alertEmail == null)
                // The value doesn't exist, the application is not set to run at startup
                return null;

            // The value exists, the application is set to run at startup
            return alertEmail;
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (!ignoreStateChange)
            {
                saveBtn.Enabled = true;
                if (checkBox2.CheckState == CheckState.Checked)
                {
                    emailTxtbox.Enabled = true;

                }
                else
                {
                    emailTxtbox.Enabled = false;
                }
            }
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            saveBtn.Enabled = false;
            SetAlertEmail();
            
            
            if (checkBox1.CheckState == CheckState.Checked)
            {
                RegistryKey rkApp = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);

                if (!IsStartupItem())
                    // Add the value in the registry so that the application runs at startup
                    rkApp.SetValue(AppName, Application.ExecutablePath.ToString());
                rkApp.Close();
            }
            else
            {
                RegistryKey rkApp = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);

                if (IsStartupItem())
                    // Remove the value from the registry so that the application doesn't start
                    rkApp.DeleteValue(AppName, false);
                rkApp.Close();
            }

            if (checkBox2.CheckState == CheckState.Checked)
                SetSendAlertEmail("1");
            else
                SetSendAlertEmail("0");
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if(!ignoreStateChange)
                saveBtn.Enabled = true;
        }
    }
}
