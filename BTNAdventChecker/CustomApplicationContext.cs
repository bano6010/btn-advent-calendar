﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Net;
using System.Windows.Forms;
using System.Timers;

namespace BTNAdventChecker
{
    public class CustomApplicationContext : ApplicationContext
    {
        private IContainer components;
        private NotifyIcon BTNNotifyIcon;       
        private ContextMenu BTNNotifyIconContextMenu;
        private MenuItem exitContextMenuItem;
        private MenuItem showContextMenuItem;
        private MenuItem resetCookiesContextMenuItem;
        private MenuItem showSettingsMenuItem;

        private TimeLeftForm mainForm;
        private SettingsForm settingsForm;

        private System.Timers.Timer timer;

        
        public CustomApplicationContext()
        {
            InitializeContext();

            String LogPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),"BTNAdventCalendarChecker/BTNAdventChecker.log");
            Log.init(LogPath);

            CloudFlareBTNClient.Init();
            if (!CloudFlareBTNClient.IsAuthenticated())
            {
                MessageBox.Show("Please log into BTN using Chrome or FireFox and make sure to check the \"Keep me logged in\" box and then restart your browser.", "Authentication Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(0);
            }
            //timer_Elapsed(null, null);

            //String sessionCookie = FindBTNCookie();
            //if (String.IsNullOrEmpty(sessionCookie)) MessageBox.Show("Got a null session cookie.");
            //else sessionID = sessionCookie;

            //if(!HaveLocalCookies()) GetLoginCookies();
            //else 
            enableTimer();
        }

        

        private void enableTimer()
        {
            timer = new System.Timers.Timer();
            timer.Interval = 1000 * 60 * 5; //5 min
            timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);
            timer.Start();
        }

        private void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            CheckCalendarWorker worker = new CheckCalendarWorker();
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
                worker.RunWorkerAsync();
        }

        private void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            String output = e.Result.ToString();
            //if (String.Compare("NotAuthenticated", output) == 0)
                //GetLoginCookies();
        }

        private void InitializeContext()
        {
            components = new Container();
            BTNNotifyIcon = new NotifyIcon(components);
            BTNNotifyIconContextMenu = new ContextMenu();
            showContextMenuItem = new MenuItem();
            exitContextMenuItem = new MenuItem();
            resetCookiesContextMenuItem = new MenuItem();
            showSettingsMenuItem = new MenuItem();

            BTNNotifyIcon.ContextMenu = BTNNotifyIconContextMenu;
            BTNNotifyIcon.DoubleClick += new EventHandler(BTNNotifyIcon_DoubleClick);
            BTNNotifyIcon.Icon = new Icon(typeof(CustomApplicationContext), "btn.ico");
            BTNNotifyIcon.Text = "BTN Advent Calendar Checker";
            BTNNotifyIcon.Visible = true;

            BTNNotifyIconContextMenu.MenuItems.AddRange(new MenuItem[] { showContextMenuItem, showSettingsMenuItem, exitContextMenuItem }); //resetCookiesContextMenuItem

            showContextMenuItem.Index = 0;
            showContextMenuItem.Text = "&Show Timer";
            showContextMenuItem.DefaultItem = true;
            showContextMenuItem.Click += new EventHandler(showContextMenuItem_Click);

            /*
            resetCookiesContextMenuItem.Index = 1;
            resetCookiesContextMenuItem.Text = "&Reset Cookies and Login";
            resetCookiesContextMenuItem.Click += new EventHandler(resetCookiesContextMenuItem_Click);
            */
             
            showSettingsMenuItem.Index = 1;
            showSettingsMenuItem.Text = "&Settings";
            showSettingsMenuItem.Click += new EventHandler(showSettingsMenuItem_Click);

            exitContextMenuItem.Index = 2;
            exitContextMenuItem.Text = "&Exit";
            exitContextMenuItem.Click += new EventHandler(exitContextMenuItem_Click);

           
        }

        private void showSettingsMenuItem_Click(object sender, EventArgs e)
        {
            showSettingsForm();
        }

        private void resetCookiesContextMenuItem_Click(object sender, EventArgs e)
        {
            //TODO check if there is already an open window, and show that if there is
            GetLoginCookies();
        }

        private void showContextMenuItem_Click(object sender, EventArgs e)
        {
            ShowForm();
        }

        private void exitContextMenuItem_Click(object sender, EventArgs e)
        {
            ExitThreadCore();
        }

        private void BTNNotifyIcon_DoubleClick(object sender, System.EventArgs e)
        {
            ShowForm();
        }

        private void ShowForm()
        {
            if (mainForm == null)
            {
                // create a fresh new form and show it.
                mainForm = new TimeLeftForm();
                mainForm.Show();

                // hook onto the closed event so we can null out the main form...  this avoids reshowing
                // a disposed form.
                mainForm.Closed += new EventHandler(mainForm_Closed);
            }
            else
            {
                // the form is currently visible, go ahead and bring it to the front so the user can interact
                mainForm.Activate();
            }
        }

        private void showSettingsForm()
        {
            if (settingsForm == null)
            {
                // create a fresh new form and show it.
                settingsForm = new SettingsForm();
                settingsForm.Show();

                settingsForm.Closed += new EventHandler(settingsForm_Closed);
            }
            else
            {
                // the form is currently visible, go ahead and bring it to the front so the user can interact
                settingsForm.Activate();
            }
        }

        void settingsForm_Closed(object sender, EventArgs e)
        {
            settingsForm = null;
        }


        private void mainForm_Closed(object sender, EventArgs e)
        {
            mainForm = null;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                    BTNNotifyIcon.Dispose();
                }
            }
        }

        protected override void ExitThreadCore()
        {
            if (mainForm != null)
            {
                // before we exit, give the main form a chance to clean itself up.
                mainForm.Close();
            }
            if(settingsForm != null)
            {
                settingsForm.Close();
            }
            Dispose();
            base.ExitThreadCore();
        }

        public void GetLoginCookies()
        {
            GetBTNCredentials credentialForm = new GetBTNCredentials();
            credentialForm.Closed += new EventHandler(credentialForm_Closed);
            credentialForm.Show();
        }

        private void credentialForm_Closed(object sender, EventArgs e)
        {
            enableTimer();
            ShowForm();
        }
    }
}
