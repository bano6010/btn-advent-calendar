﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BTNAdventChecker
{
    class NotAuthenticatedException : Exception
    {
        private String Msg = null;
        public NotAuthenticatedException(String msg)
        {
            Msg = msg;
        }
    }
}
