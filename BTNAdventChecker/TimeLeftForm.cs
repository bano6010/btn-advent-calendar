﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace BTNAdventChecker
{
    public partial class TimeLeftForm : Form
    {
        private CheckCalendarWorker worker;

        public TimeLeftForm()
        {
            InitializeComponent();

            progressBar1.Style = ProgressBarStyle.Marquee;

            worker = new CheckCalendarWorker();
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
            worker.RunWorkerAsync();
        }

        private void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            progressBar1.Visible = false;

            String output = e.Result.ToString();

            if (String.Compare("An exception occurred during a WebClient request.", output) == 0)
            {
                label1.Text = "There was a connection issue.\nCheck your internet connection and try again.";
                return;
            }

            if (String.IsNullOrEmpty(output))
            {
                label1.Text = "Could not connect to BTN.";
                return;
            }

            Match data = Regex.Match(output,@"Sorry, Too Early!</h2><br><br>You have already claimed a prize today.<br><br>Please try again in <b>(\d)+d (\d)+h (\d)+m (\d)+s</b>.");
            if (data.Success) //Not Time, lets parse the remaing time out
            {
                Match timeLeft = Regex.Match(data.Value, @"(\d)+d (\d)+h (\d)+m (\d)+s");
                label1.Text = timeLeft.Value;
                return;
            }

            Match checkForLogin = Regex.Match(output,@"<strong>WARNING:</strong> You will be banned for 6 hours after your login attempts run out.<br /><br />");
            if (checkForLogin.Success)
            {
                label1.Text = "There was an error with your credentials!";
                label2.Visible = true;
                return;
            }

            Match checkForMaintenance = Regex.Match(output, "BTN: Down for \"Maintenance\"");
            if (checkForMaintenance.Success)
            {
                label1.Text = "BTN is down for \"Maintenance\"";
                return;
            }

            label1.Text = "You got a prize!";
            //System.IO.File.WriteAllText("maintance.html", output);
        }
    }

    
}
