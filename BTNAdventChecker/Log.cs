﻿using System;
using System.IO;

namespace BTNAdventChecker
{
    class Log
    {
        private static String LogPath = String.Empty;
        public static void init(String Logpath)
        {
            Log.LogPath = Logpath;
            if (!Directory.Exists(Path.GetDirectoryName(Log.LogPath)))
                Directory.CreateDirectory(Path.GetDirectoryName(Log.LogPath));
            if (!File.Exists(Log.LogPath))
                File.Create(Log.LogPath);
        }

        public static void Debug(String s)
        {
            if (String.IsNullOrEmpty(Log.LogPath)) return;

            String formatedString = String.Format("{0} DEBUG: {1}{2}", DateTime.Now.ToString(), s, Environment.NewLine);
            File.AppendAllText(Log.LogPath, formatedString);
        }

        public static void WriteLine()
        {
            if (String.IsNullOrEmpty(Log.LogPath)) return;
            File.AppendAllText(Log.LogPath, Environment.NewLine);
        }

        public static void Error(String s)
        {
            if (String.IsNullOrEmpty(Log.LogPath)) return;

            String formatedString = String.Format("{0} ERROR: {1}{2}", DateTime.Now.ToString(), s, Environment.NewLine);
            File.AppendAllText(Log.LogPath, formatedString);
        }


    }
}
