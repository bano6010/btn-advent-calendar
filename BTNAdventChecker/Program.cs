﻿using System;
using System.Windows.Forms;

#if DEBUG
using System.Runtime.InteropServices;
#endif

namespace BTNAdventChecker
{
    static class Program
    {

#if DEBUG
        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool AllocConsole();
#endif

        [STAThread]
        static void Main()
        {

#if DEBUG
            AllocConsole();
#endif

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            CustomApplicationContext applicationContext = new CustomApplicationContext();
            Application.Run(applicationContext);
        }
    }
}
