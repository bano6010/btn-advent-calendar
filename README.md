BTN Advent Calendar Checker

A simple advent calendar checker for BTN.

This app requires the .NET 4.5 Framework to run.

It does not store or transmit your credentials anywhere except to generate a StayLoggedIn cookie from the BTN login page. This cookie is stored in an XML file on your disk and is referenced when checking your advent calendar prize status.

NOTE: The app will FAIL to log in when BTN has capcha verification enabled. Wait until capcha is no longer required to log in and try again. If you have already logged in and generated your cookie, it will work when checking for a prize.

Simply copy the BTNAdventChecker Folder somewhere on your computer (like your desktop) and run the .exe inside.

After that check your task tray for a BTN icon and double click it to check for a prize. The app also checks every 5 min when running in the task bar.

If your account is not ready to get a prize the window will show you how long until you can claim one. The windows does not update on its own yet, simply close and reopen the window to update your time.

This is still an early version I hacked out as fast I could. Check https://bitbucket.org/bano6010/btn-advent-calendar for updates in the coming days.

To enable automatic startup on boot, select the option from the Settings menu.

This app is licensed under MIT, see LICENSE for details. I would be happy to receive pull requests so if it suits you, feel free.